import os
import random

def rename_multi_files(end):
    image_folder = os.getcwd()+ '/data/shelves_images/'
    xml_folder = os.getcwd()+ '/data/shelves_images_XML/'
    list_images = os.listdir(image_folder)
    list_xml = os.listdir(xml_folder)
    for file_name in list_images:
        if (file_name.split('.')[0] + '.xml') in list_xml:
            continue
        src = image_folder + file_name
        dst = image_folder + str(random.randint(0,end))
        while(os.path.isfile(dst + '.jpg')):
            dst = image_folder + str(random.randint(0,end))
        os.rename(src=src, dst=dst + '.jpg')
    return True
