import tensorflow as tf
import numpy as np
from utils import visualization_utils, label_map_util
import cv2
from box import Box
import urllib

PATH_TO_MODEL = '../workspace/training_demo/training/fine_tuned_model/frozen_inference_graph.pb'
PATH_TO_LABEL_MAP = '../workspace/training_demo/annotations/label_map.pbtxt'

class EmptySpaceDetector:
    def __init__(self):
        self.detection_graph = tf.Graph()
        with self.detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            # Works up to here.
            with tf.gfile.GFile(PATH_TO_MODEL, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')
            # Definite input and output Tensors for detection_graph
            self.image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
            # Each box represents a part of the image where a particular object was detected.
            self.detection_boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
            self.detection_scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
            self.detection_classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
            self.num_detections = self.detection_graph.get_tensor_by_name('num_detections:0')
        self.sess = tf.Session(graph=self.detection_graph)
        self.category_index = self.load_label_map()

    def load_label_map(self):
        label_map = label_map_util.load_labelmap(PATH_TO_LABEL_MAP)
        categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=1, use_display_name=True)
        category_index = label_map_util.create_category_index(categories)
        return category_index

    def get_classification(self, img_np):
        # img_np = cv2.imread(img_path)
        # Bounding Box Detection.
        with self.detection_graph.as_default():
            # Expand dimension since the model expects image to have shape [1, None, None, 3].
            img_expanded = np.expand_dims(img_np, axis=0)
            (boxes, scores, classes, num) = self.sess.run(
                [self.detection_boxes, self.detection_scores, self.detection_classes, self.num_detections],
                feed_dict={self.image_tensor: img_expanded})
        return boxes, scores, classes, num

    def get_detected_box_infos(self, boxes, scores, classes, max_score, img_np):
        image_demention = img_np.shape
        im_height, im_width= image_demention[0], image_demention[1]

        list_boxs = []
        for i in range(len(boxes)):
            if(scores[0][i] >= max_score):
                box = Box()
                box.score = float(scores[0][i])
                box.label = self.category_index[int(classes[0][i])]['name']
                (box.top, box.left, box.bottom, box.right) = (int(boxes[0][i][0] * im_height), int(boxes[0][i][1] * im_width), int(boxes[0][i][2] * im_height), int(boxes[0][i][3] * im_width))
                list_boxs.append(box)
        return  list_boxs

    def draw_boxs_on_image(self, img_path, output_path):
        img_name = img_path.split('/')[-1]
        img_np = cv2.imread(img_path)
        boxes, scores, classes, num = self.get_classification(img_np)

        visualization_utils.visualize_boxes_and_labels_on_image_array(
            img_np,
            np.squeeze(boxes),
            np.squeeze(classes).astype(np.int32),
            np.squeeze(scores),
            self.category_index,
            groundtruth_box_visualization_color='red',
            use_normalized_coordinates=True,
            line_thickness=3)
        cv2.imwrite(output_path + '/' + 'processed_' + img_name, img_np)
        print("Boxs were drown on image.")
        return ('processed_' + img_name)

    def draw_boxs_on_video(self, video_input_path = 0, video_output_path = "real_time_space_detection.avi"):
        # cap = cv2.VideoCapture(video_input_path)
        cap = cv2.VideoCapture()
        cap.open(video_input_path)

        cap.set(cv2.CAP_FFMPEG,True)
        codec = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')

        if cap.isOpened():
            # if video_input_path != 0 and video_input_path != 1:
            #     # get vcap property
            #     width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)  # float
            #     height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)  # float
            #     resolution = (int(width), int(height))
            #     # # or
            #     # width = cap.get(3)  # float
            #     # height = cap.get(4)  # float
            #     frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
            #     frame_rate = int(cap.get(cv2.CAP_PROP_FPS))
            # else:
            #     resolution = (640,480)
            #     frame_rate = 30

            # get vcap property
            width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)  # float
            height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)  # float
            resolution = (int(width), int(height))
            # # or
            # width = cap.get(3)  # float
            # height = cap.get(4)  # float
            frame_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
            frame_rate = int(cap.get(cv2.CAP_PROP_FPS))


        VideoFileOutput = cv2.VideoWriter(video_output_path, codec, frame_rate, resolution)

        ret = True
        i = 0


        while (ret):
            try:
                # Definite input and output Tensors for detection_graph
                ret, img_np = cap.read()
                boxes, scores, classes, num = self.get_classification(img_np)
                # detected_box_infos = self.get_detected_box_infos(boxes, scores, classes, 0.5,img_np)
                # print(detected_box_infos)
                visualization_utils.visualize_boxes_and_labels_on_image_array(
                    img_np,
                    np.squeeze(boxes),
                    np.squeeze(classes).astype(np.int32),
                    np.squeeze(scores),
                    self.category_index,
                    groundtruth_box_visualization_color='red',
                    use_normalized_coordinates=True,
                    line_thickness=3)
                VideoFileOutput.write(img_np)


                # if video_input_path != 0 and video_input_path != 1:
                #     i = i + 1
                #     if i % 100 == 0:
                #         print(str(round(i / frame_count * 100)) + '%')
                #
                # else:
                #     cv2.imshow('live_detection', img_np)
                #     if cv2.waitKey(25) & 0xFF == ord('q'):
                #         break
                #         cv2.destroyAllWindows()
                #         cap.release()

                cv2.imshow('live_detection', img_np)
                if cv2.waitKey(25) & 0xFF == ord('q'):
                    break
                    cv2.destroyAllWindows()
                    cap.release()


            except Exception as e:
                print(str(e))
                pass
        print("Video created.")