import helper
from EmptySpaceDetector import EmptySpaceDetector
import os
from flask import Flask, render_template, redirect, url_for, request, flash, jsonify
from werkzeug.utils import secure_filename
app = Flask(__name__)

detector = EmptySpaceDetector()
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
UPLOADED_FOLDER = 'static/uploaded_images'

@app.route('/home')
def index():
    return render_template("index.html")


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/upload_file', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            return jsonify({'message':'No file part.'})
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            return jsonify({'message':'No selected file.'})
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(UPLOADED_FOLDER + '/' + filename)
            return jsonify({'message': filename + ' was uploaded.'})
            # return redirect(url_for('home'))

@app.route('/load_image/<image_name>')
def load_image(image_name):
    return render_template("display.html", image_name=image_name)

@app.route('/detect_empty_space_in_image/<image_name>')
def get_detected_box_infos(image_name):
    input_dir = 'static/uploaded_images'
    output_dir = 'static/output'
    output_image_name = detector.draw_boxs_on_image(input_dir + '/' + image_name, output_dir)
    # return redirect(url_for('load_image', output_dir=output_dir, image_name = output_image_name))
    return jsonify({"output_image_name":output_image_name})




def main():
    # dir = 'C:/Users/trltr/Documents/KLTN/Tensorflow/workspace/training_demo/images/data/shelves_images'

    # ------------------------------DETECT EMPTY SPACE IN IMAGE------------------------------
    input_dir = 'images/test'
    output_dir = 'images/test_output'
    # list_images = os.listdir(input_dir)
    # for img in list_images:
    #     boxes, scores, classes, num = detector.get_classification(input_dir + '/' + img)
    #     list_box = detector.get_detected_box_infos(boxes=boxes, scores=scores,classes=classes, max_score=0.5, img_path=input_dir + '/' + img)
    #     print ()
    #     detector.visualize_boxs(input_dir + '/' + img, output_dir)

    # ------------------------------DETECT EMPTY SPACE IN VIDEO------------------------------
    video_input_path = 'videos/Inside an Italian supermarket.mp4'
    # video_input_path = 'http://192.168.43.46:8080/video'
    # video_input_path = 'http://192.168.4.110:8080/video'
    # detector.draw_boxs_on_video(video_input_path, video_output_path = "real_time_space_detection_3.avi")

if __name__ == '__main__':
    app.run(host="127.0.0.1", port="5050", debug=True, threaded=True)